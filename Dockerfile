FROM openjdk:8-alpine

# adds the app dir to the container
ADD https://gitlab.com/theflockers/mta-host-optimizer/-/package_files/262155/download mta-host-optimizer.jar

CMD exec java -jar mta-host-optimizer.jar
