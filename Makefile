SERVICE_NAME=mta-host-optimizer
PACKAGE_NAME=mta-host-optimizer
VERSION=0.0.1-SNAPSHOT
JAVA_HOME=
DOCKER_TAG_VER=$(shell date +"%Y%m%d.%H%M")
DESTDIR=app/
IFACE=enp4s0
REGISTRY_URL=registry.gitlab.com
REGISTRY_USER=theflockers
ADVERTISE_ADDR=$(shell ip addr show dev enp4s0 | grep inet |grep -v inet6 |awk '/192/ {print substr($$2,0, index($$2, "/")-1)}')

all: build

build: 
	JAVA_HOME=${JAVA_HOME} ./mvnw versions:set -DnewVersion=${DOCKER_TAG_VER}-SNAPSHOT
	JAVA_HOME=${JAVA_HOME} ./mvnw versions:update-child-modules
	JAVA_HOME=${JAVA_HOME} ./mvnw dependency:resolve
	JAVA_HOME=${JAVA_HOME} ./mvnw compiler:compile -Dmaven.install.skip=true
	JAVA_HOME=${JAVA_HOME} ./mvnw package -Dmaven.install.skip=true
	JAVA_HOME=${JAVA_HOME} ./mvnw deploy -s ci_settings.xml -Dmaven.install.skip=true -DpushChanges=false
	#install -D target/${PACKAGE_NAME}-${}.jar app/${PACKAGE_NAME}.jar
run:
	JAVA_HOME=${JAVA_HOME} ./mvnw spring-boot:run

release: build
	JAVA_HOME=${JAVA_HOME} ./mvnw release:prepare -Dmaven.javadoc.skip=true
	JAVA_HOME=${JAVA_HOME} ./mvnw release:perform -s ci_settings.xml -Darguments="-Dmaven.javadoc.skip=true"
	JAVA_HOME=${JAVA_HOME} ./mvnw deploy -s ci_settings.xml
clean:
	JAVA_HOME=${JAVA_HOME} ./mvnw clean
	rm -rf app/

# this is meant to local build
# productio build is done by gitlab-ci kaniko build
docker: package
	ln -f Dockerfile ${DESTDIR}
	docker build -f ${DESTDIR}Dockerfile . -t ${REGISTRY_URL}/${REGISTRY_USER}/${SERVICE_NAME}:latest -t ${REGISTRY_URL}/${REGISTRY_USER}/${SERVICE_NAME}:v${DOCKER_TAG_VER}

push:
	docker push ${REGISTRY_URL}/${REGISTRY_USER}/${SERVICE_NAME}:latest
	docker push ${REGISTRY_URL}/${REGISTRY_USER}/${SERVICE_NAME}:v${DOCKER_TAG_VER}

# deploy docker stack
swarm: docker
	docker swarm init --advertise-addr ${ADVERTISE_ADDR} &>/dev/null
	sleep 5
	docker stack deploy --compose-file docker-compose.yml ${SERVICE_NAME}

swarm-reset:
	docker swarm leave --force &>/dev/null
	docker stack rm ${PACKAGE_NAME}
	sleep 5

service-update:
	docker service update ${SERVICE_NAME}_${PACKAGE_NAME}

test:
	JAVA_HOME=${JAVA_HOME} ./mvnw test
