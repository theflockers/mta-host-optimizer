[![pipeline status](https://gitlab.com/theflockers/mta-host-optimizer/badges/master/pipeline.svg)](https://gitlab.com/theflockers/mta-host-optimizer/commits/master)

# MTA Host Optimizer

## considerations
To build the server you can use the _Makefile_ through the `make` command and also to make a local build/deployment.

The Pipeline is called in two situations: _commits_ and _tags_.

- For _commits_: It runs the _test_ job and _build_
- For _tags_: It runs the _test_ and _release_ and _docker_ jobs.

It pushes the image to _Gitlab repository_ inside the Gitlab project

It also contains the *k8s* yaml file that allows the installation on a Kubernetes Cluster.

## Not complete

- Tests coverage

## Others

- I tried for a while make _kaniko_ build but without success. So it's using docker in docker build.

# Pipeline Build

For Pipeline build you just need to commit a change - it will trigger build and tests - or create and push a new tag. For instance:
```
$ git tag $(date +"%Y%m%d.%H%M")
$ git push --tags
```
This will it will automatically start the *release* Pipeline.

# Local Build

## Build
This step downloads the deps and builds the software.
```
$ make build
```
## Test
Runs unit tests
```
$ make test
```
## Creating one release
This step creates the release version and pushes o Gitlab Maven repo to be used by Docker build.
```
$ make release DOCKER_TAG_VER=${TAG_NAME} 
```

## Create docker artifact
This step creates the jar package and runs the docker routine to build the new image. It also pushes the image (needs to be authenticated) to the defined `DOCKERHUB_USER` variable.
In the pipeline it builds through *kaniko*. See _.gitlab-ci.yml_.
```
$ make docker
```
## Stack
Creates the new service with load balancing enabled.
```
$ make swarm
```
Cleans up the created stack.
```
$ make swarm-reset
```

# Docker & Rancher

The `docker-compose.yml` and `rancher-compose.yml` files are contained and they should be usable in a Rancher 1.6 server as tested. 

# Kubernetes
This can be also deployed as a Deamonset on Kubernetes.
```
$ cd k8s/
$ kubectl create -f mta-optimizer.yml
```

