/**
 * MtaHostsOptimizerApplication is a service that gets the inefficient (or degraded)
 * hosts according with the X variable (threshold).
 *
 * @author Leandro Mendes<theflockers@gmail.com>
 * @version 1.0
 */
package info.lmendes.mtahostoptimizer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MtaHostOptimizerApplication {
    /**
     * application bootstrap
     */
    public static void main(String[] args) {
        SpringApplication.run(MtaHostOptimizerApplication.class, args);
    }
}
