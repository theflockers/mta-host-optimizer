/**
 * ipConfigController is the main REST controller for this project
 * 
 * @author Leandro Mendes<theflockers@gmail.com>
 * @version 1.0
 */
package info.lmendes.mtahostoptimizer.controller;

import org.springframework.stereotype.Controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PathVariable;

import info.lmendes.mtahostoptimizer.model.ipConfig;
import info.lmendes.mtahostoptimizer.service.ipConfigService;
import java.util.List;
import java.util.ArrayList;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.LinkedHashSet;

@Controller
public class ipConfigController {
  /**
   * public class ipConfigController
   * 
   * @param ipConfigService ipConfigService
   *
   * @method public List getDegradedMtaHosts()
   * @method public List getInefficientMtaHosts()
   * @method public List getIpConfigByHostFqdn()
   * @method public List getIpConfig()
   */
  @Autowired
  ipConfigService ipConfigService;

  /**
   * public List getDegradedMtaHosts()
   * Renders all Degraded MTA Hosts 
   * @return List<ArrayList>
   */
  @GetMapping("/degraded")
  @ResponseBody
  public List getDegradedMtaHosts() {
    List configs = new ArrayList();
    ipConfigService.getDegradedMtaHosts().forEach(config -> configs.add(config.getHostfqdn()));


    LinkedHashSet<Integer> hashSet = new LinkedHashSet<>(configs);
    List configsUndup = new ArrayList<>(hashSet);
    return configsUndup;
  }

  /**
   * public List getInefficient()
   * Alias for getDegradedMtaHosts()
   * @return List
   */
  @GetMapping("/inefficient")
  @ResponseBody
  public List getInefficientMtaHosts() {
    return this.getDegradedMtaHosts();
  }
  
  /**
   * public ipConfig getIpConfigById()
   * Return ipConfig according to the defined id
   * @return ipConfig
   */
  @GetMapping("/ipconfig/{id}")
  @ResponseBody
  public ipConfig getIpConfigById(@PathVariable("id") Integer id) {
    return ipConfigService.getIpConfigById(id);
  }

  /**
   * public List<ipConfig> getIpConfig()
   * Return ALL ipConfig objects
   * @return List
   */
  @GetMapping("/ipconfig")
  @ResponseBody
  public List<ipConfig> getIpConfig() {
    return ipConfigService.getAllIpConfig();
  }
}
