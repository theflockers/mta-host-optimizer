package info.lmendes.mtahostoptimizer.misc;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ipConfigData {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public void bootstrap() {
        this.createTable();
        this.populate();
    }

    private void createTable() {
        jdbcTemplate.execute("DROP TABLE ipconfig IF EXISTS");
        jdbcTemplate.execute("CREATE TABLE ipconfig(" + 
            "id       SERIAL, " +
            "hostfqdn VARCHAR(255) NOT NULL, " +
            "ip       VARCHAR(15)  NOT NULL, " + 
            "active   BOOLEAN      NOT NULL DEFAULT false)");
    }
    private void populate() {
        List<Object[]> hostMap = Arrays.asList(
            "127.0.0.1:mta-prod-1:true",
            "127.0.0.2:mta-prod-1:false",
            "127.0.0.3:mta-prod-2:true",
            "127.0.0.4:mta-prod-2:true",
            "127.0.0.5:mta-prod-2:false",
            "127.0.0.6:mta-prod-3:false")
            .stream()
            .map(entry -> entry.split(":"))
            .collect(Collectors.toList());
         
        jdbcTemplate.batchUpdate("INSERT INTO ipconfig (hostfqdn, ip, active) " +
            "VALUES (?, ?, ?)", hostMap);
    }
}
