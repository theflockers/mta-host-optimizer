/**
 * ipConfig class is basic model for database abstraction
 *
 * @author Leandro Mendes<theflockers@gmail.com>
 * @version 0.1
 */

package info.lmendes.mtahostoptimizer.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.NamedQuery;

/* sets the table name */
@Table(name="ipconfig")
@Entity
public class ipConfig {
  /**
   * Abstracts the ipConfig table to the Model using jpa for abstraction
   *
   * @param Integer id
   * @param String  hostfqdn
   * @param String  ip 
   * @param Boolean active
   *
   * @method public Int     getId()
   * @method public String  getHostfqdn()
   * @method public String  getIp()
   * @method public Boolean getActive()
   */
  @Id
  @GeneratedValue
  private Integer id;
  private String  hostfqdn;
  private String  ip;
  private Boolean active;
  
  /**
   * returns the object id
   * @return Integer id
   */
  public Integer getId() {
    return id;
  }
  /**
   * returns the object hostfqdn
   * @return String hostfqdn
   */ 
  public String getHostfqdn() {
    return hostfqdn;
  }
  /**
   * returns the object ip
   * @return String ip
   */
  public String getIp() {
    return ip;
  }
  /**
   * returns the object active status
   * @return Boolean active
   */
  public Boolean getActive() {
    return active;
  }
  
}
