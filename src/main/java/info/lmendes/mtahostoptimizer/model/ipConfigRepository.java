/**
 * ipConfigRepository is a public interface that extends CrudRepository 
 * to abstract ipConfig
 *
 * @author Leandro Mendes<theflockers@gmail.com>
 * @version 1.0
 * @see CrudRepository
 */
package info.lmendes.mtahostoptimizer.model;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.jpa.repository.Query;

public interface ipConfigRepository extends CrudRepository<ipConfig, Integer> {
    /**
     * Special query to get only the degraded hosts - it needs to mimic the ipConfig model for `jpa` render the Model
     * properly. It get a new ID from a dummy sequece only to not colide id as it returns a pseudo object for ipConfig
     */
    @Query(value = "SELECT NEXTVAL('dummy_sequence') as id, hostfqdn, '169.254.0.1' as ip, TRUE as active, COUNT(*) ac_mtas FROM ipconfig " +
                   "WHERE active=TRUE "    +
                   "GROUP BY hostfqdn "    +
                   "HAVING ac_MTAS <= ?1 " +
                   "UNION "                +
                   "SELECT NEXTVAL('dummy_sequence') as id, hostfqdn, '169.254.0.1' as ip, TRUE as active, COUNT(*) ac_mtas FROM ipconfig " + 
                   "GROUP BY hostfqdn "    +
                   "HAVING ac_mtas <= ?1", nativeQuery = true)
    /**
     * List<ipConfig> findHostsByThreshold(Integer threshould)
     * Implements the above query to find the mta hosts running out of capacity.
     */
    List<ipConfig> findHostsByThreshold(Integer threshold);
}
