/**
 * ipConfigService class is a Service class to fetch information from ipConfigRepository (jpa)
 *
 * @author Leandro Mendes<theflockers@gmail.com>
 * @version 0.1
 * @see ipConfigRepository
 */

package info.lmendes.mtahostoptimizer.service;

import info.lmendes.mtahostoptimizer.model.ipConfig;
import info.lmendes.mtahostoptimizer.model.ipConfigRepository;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Optional;
import java.util.ArrayList;

@Service
public class ipConfigService {
    /**
     * Defines methods to be used by the Controller 
     *
     * @param String             threshold 
     * @param Integer            x_threshold
     * @param ipConfigRepository ipConfigRepository
     *
     * @method public Int     getId()
     * @method public String  getHostfqdn()
     * @method public String  getIp()
     * @method public Boolean getActive()
     */

    /* threshold variable is gotten from the application.properties */
    @Value("${threshold}")
    protected String threshold;

    /* default value for the threshold variable if not set */
    protected Integer x_threshold = 1;

    @Autowired
    ipConfigRepository ipConfigRepository;
   
    /**
     * List<ipConfig> getAllIpConfig()
     *
     * Returns all ipConfig entries
     *
     * @return List<configs>
     */
    public List<ipConfig> getAllIpConfig() {
        List<ipConfig> configs = new ArrayList<ipConfig>();
        ipConfigRepository.findAll().forEach(config -> configs.add(config));
        return configs;
    }

     /**
     * List<ipConfig> getDegradedMtaHosts()
     *
     * Returns all hostnames (not fqdn besides the name) from the database 
     * having less or equals X (threshold) active IPaddresses exists
     *
     * @return List<configs>
     */
    public List<ipConfig> getDegradedMtaHosts() {

        if( threshold != null ) {
            this.setXThreshold(Integer.valueOf(threshold));
        }

        List<ipConfig> configs = new ArrayList<ipConfig>();
        ipConfigRepository.findHostsByThreshold(this.getXThreshold()).forEach(config -> configs.add(config));
        return configs;
    }

    /**
     * ipConfig getIpConfigById(Integer id)
     *
     * Returns hostnames (not fqdn) from the database with id = id 
     *
     * @return ipConfig
     */   
    public ipConfig getIpConfigById(Integer id) {
        ipConfig config;
	config = ipConfigRepository.findById(id).get();
        return config;
    }
    /**
     * void setXThreshold(Integer threshold)
     * 
     * sets the X threshold 
     * @see ipConfigServer.getDegradedMtaHosts()
     */   
    protected void setXThreshold(Integer threshold) {
        this.x_threshold = threshold;
    }
    
    /**
     * Integer getXThreshold()
     *
     * gets the X threshold 
     * @return Integer x_threshold
     */
    protected Integer getXThreshold() {
        return x_threshold;
    }
}
