DROP TABLE ipconfig IF EXISTS;
CREATE TABLE ipconfig (
    id       SERIAL, 
    hostfqdn VARCHAR(255) NOT NULL,
    ip   VARCHAR(15)  NOT NULL,
    active   BOOLEAN      NOT NULL DEFAULT false
);

INSERT INTO ipconfig (ip, hostfqdn, active) VALUES 
    ( '127.0.0.1', 'mta-prod-1', true  ),
    ( '127.0.0.2', 'mta-prod-1', false ),
    ( '127.0.0.3', 'mta-prod-2', true  ),
    ( '127.0.0.4', 'mta-prod-2', true  ),
    ( '127.0.0.5', 'mta-prod-2', false ),
    ( '127.0.0.6', 'mta-prod-3', false );

-- dummy sequence for pseudo id generation
CREATE SEQUENCE dummy_sequence start with 100;
